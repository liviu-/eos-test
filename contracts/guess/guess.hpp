#include<eosiolib/eosio.hpp>
#include<eosiolib/transaction.hpp>
#include<string>

class guess : eosio::contract {
    public:
        guess( account_name account ) : eosio::contract(account) {}

        void guess_bool( account_name account, bool my_guess);
};

