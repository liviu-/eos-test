#include<eosiolib/eosio.hpp>
#include<string>

using std::string;

class controller : public eosio::contract {
    public:
        controller(account_name account) : eosio::contract(account) {}

        void talk(account_name account, string message);
        void store(account_name account, string input, string output);
        void guess(account_name account, bool my_guess);
};
